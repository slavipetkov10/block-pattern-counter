import 'dart:async';

import './counter_event.dart';

class CounterBloc {
  int _counter = 0;

  final _counterStateController = StreamController<int>();
  // The sink is private, it can be used in this this class only
  // We will put numbers in the sink _inCounter and they will come out
  // from the counter stream
  StreamSink<int> get _inCounter => _counterStateController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get counter => _counterStateController.stream;


  final _counterEventController = StreamController<CounterEvent>();
  // For events, exposing only a sink which is an input
  Sink<CounterEvent> get counterEventSink => _counterEventController.sink;

  CounterBloc() {
    /// 2
    // Whenever there is a new event, we want to map it to a new state
    // From the stream are coming out CounterEvent objects depending on the
    // pressed button , either IncrementEvent or decrement. The event goes to
    // the function _mapEventToState which is listening on the stream
    _counterEventController.stream.listen(_mapEventToState);
  }

  /// 3
  void _mapEventToState(CounterEvent event){
    if(event is IncrementEvent)
        _counter++;
    else
        _counter--;

    /// 4 a number is added in the counter sink
    _inCounter.add(_counter);
  }

  void dispose() {
    _counterStateController.close();
    _counterEventController.close();
  }

}
























